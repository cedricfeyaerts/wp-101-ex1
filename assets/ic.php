/*************************************************************************/
//Contenu dans le JS de la page aha
/*************************************************************************/

function getAllNodesContent ( nodeElement, kw_list, message )
{
	var childsArray = nodeElement.childNodes;
	var pass = 1;
	var returnValue = "unlocked";

	for(var i = 0; i < childsArray.length; i++){
		if ( childsArray[i].nodeName != "SCRIPT" && childsArray[i].nodeName != "IFRAME" && childsArray[i].nodeName != "IMG" && childsArray[i].nodeName != "A" ) {
			/*if ( childsArray[i].nodeName == "A" )
			{
				pass = 0;
				if ( window.location.host == childsArray[i].host ){
					pass = 1;
				}
			}*/
			if ( pass == 1 ){
				if(childsArray[i].hasChildNodes()){
					returnValue = getAllNodesContent ( childsArray[i], kw_list, message );
					if ( returnValue == "locked" ){
						return "locked";
					}
				}else {
					if ( childsArray[i].nodeName == "#text" ) {
						returnValue = getAllWordsFromText ( childsArray[i].textContent, kw_list, message , "content");
						if ( returnValue == "locked" ){
							return "locked";
						}
					}
				}
			}
		}	
	}
	if ( document.body == nodeElement )
	{
	    var url_words = new Array();
            var str = document.location.href;
            var res1 = str.split("-");
            for(var i= 0; i < res1.length; i++)
            {
                var res2 = res1[i].split("_");
                for(var j= 0; j < res2.length; j++)
                {
                    var res3 = res2[j].split(".");
                    for(var k= 0; k < res3.length; k++)
                    {
                        var res4 = res3[k].split("/");
                        for(var l= 0; l < res4.length; l++)
                        {
                            var res5 = res4[l].split("&");
                            for(var m= 0; m < res5.length; m++)
                            {
                                var res6 = res5[m].split("=");
                                for(var n= 0; n < res6.length; n++)
                                {
                                    if ( typeof(res6[n]) != "undefined" && res6[n] != "" && res6[n] != "\n" ) {
                                        url_words.push(res6[n].replace("%20", " ").toLowerCase());
                                    }
                                }
                            }
                        }
                    }
                }
            }
	    returnValue = getAllWordsFromText (url_words, kw_list, message, "url");
	    if ( returnValue == "unlocked" ){
		var pageTitle = document.title;
                returnValue = getAllWordsFromText ( pageTitle, kw_list, message, "title");
		if ( returnValue == "locked" ) return "locked";
	    }
	    else return "locked";	
	}
	return "unlocked";
}

// sample mode Array contient les mots de l'url. sample en string est un bloc de test
function getAllWordsFromText (sample, array_words, message, type) 
{
	// remplacement de tous les signes de ponctuation (suite de signes ou signe isolé) par un whitespace
	if(typeof sample == "object") contenu = sample;
	else contenu = (sample.toLowerCase()).replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]+/g, ' ');
	
	var blocking_keyword = "";
	var blocking_keywords_nb = array_words.length;

	for ( var i = 0; i < blocking_keywords_nb; i ++ ) {

                var word = array_words[i];
                var word_splitted = word.split("+");
		//tous les mots de la combinaison doivent etre dans le texte
                if( word_splitted.length > 1 ){

                    var nb_occ   = 0;
                    for ( var j = 0; j < word_splitted.length; j ++ ) {
			final_word = (typeof sample !== "object") ? " "+word_splitted[j].toLowerCase()+" " : word_splitted[j].toLowerCase();
                        nb_occ += contenu.indexOf(final_word) > 0 ? 1 : 0;
                    }
                    if(nb_occ  == word_splitted.length) blocking_keyword = word;
                }
		//mot simple
		else{
		    final_word = ( typeof sample !== "object") ? " "+word.toLowerCase()+" " : word.toLowerCase();
                    if( contenu.indexOf(final_word) >= 0 ) blocking_keyword = word;
                }

		if(blocking_keyword){
			//bloquer les publicités
			message += "&alerte_desc="+type+":"+word;
                        useFirewallForcedBlock(message);
                        return "locked";
		}
        }	
  	return "unlocked";
}	

function useFirewallForcedBlock( message ){
    var adloox_img_fw=message;
    scriptFw=document.createElement("script");
    scriptFw.src=adloox_img_fw;
    document.body.appendChild(scriptFw);
}
/*************************************************************************/
var firstNode = document.body;
var contentTab = ["4u9525","a mala pena legale","À peine légal","aanval","aardbeving","aatank","aatankvaad","aatankvaadi","abartig","abdeslam","abgeknallt","abgeschossen","abschaum","absturz","absturzgebiet","absturzregion","abu gunung berapi","abu muhammad al-adnani","abuse+groped","abuse+torture","accident","aceh","adel kermiche","aereo","aérien","affaire","affare","ah5017","airasia+crash","airbus","airbus a320","airbus a321-200","al qaeda","alan giese","alcohol abuse","alcohol addict","alcohol addiction","alcohol dependence","alcohol+attack","ali sonboly","alps plane crash","alter wichser","alton sterling","am strich","anal","anders behring breivik","anders breivik","andrei karlov","andrew tosh","andrey gennadyevich karlov","angegriffen","angriff","anis amri","anjem choudary","antenne","anti semitic","apartheid","apraadhi","arma","arme","arnaque","arresteren","arresto","arrêter","arsch","arschfotze","arschgesicht","arschloch","arsehole","aschewolke","ashlil sahity","asino","ass+bag","ass+bandit","ass+banger","ass+face","ass+fuck","ass+fucker","ass+goblin","ass+groped","ass+hat","ass+head","ass+hole","ass+hopper","ass+jacker","ass+lick","ass+licker","ass+monkey","ass+munch","ass+muncher","ass+nigger","ass+pirate","ass+sex","ass+shit","ass+shole","ass+sucker","ass+wad","ass+wipe","assbite","assclown","asscock","asscracker","aswolk","atom","atombombe","atomrakete","atomunfall","atr-72+taiwan","atr42-300+pakistan","attacco","attacke","attaque","aufgeilen","awan abu","babla","bacha chod","bahan letup","bahrun naim","bam","bamavarshak","banda aceh","bangkok bomb","bankrap","bastard","bataclan concert hall","bawahumur","bayaran balik","bdsm","behead","beheaded","beheading","belästigen","belästigung","bellevue","bencana","berak","beschiss","betrug","bewaffnet","bewaffnete","bilal anwar kasi","bilal hadfi","bimbos","bitch","bitchass","bitches","bitchtits","bitchy","bjs","black box","blasen","blow job","blowjob","boing","boîte noire","boko haram","bollocks","bollox","bom","bom-bom","bomb+alert","bomb+attack","bomba","bombardamenti","bombardement","bombardier","bombardiere","bombardierung","bombe","bomben","bomber","bombes","bommen","bommenwerper","bondage","boner","brahim abdeslam","breitscheidplatz","breitscheidplatz+attack","brodie copeland","brother fucker","bukkake","bulle","bullshit","bullying","bumble fuck","bumsen","bumslokal","buntut","burn+arson","busen","butt plug","buttfucka","buttfucker","cadu gaucho","camel toe","car crash","car+dead","car+death","car+killed","car+kills","carpet muncher","catastrofe","catastrophe","cendre volcanique","cenere vulcanica","chamseddine al-sandi","chapecoense+crash","cheek","cherif kouachi","child+abduction","chod","chodna","chooth","christos louvros","chut","chutiya","clit face","clit fuck","cluster fuck","cocain","cock+ass","cock+bite","cock+burger","cock+dick","cock+face","cock+fucker","cock+head","cock+jockey","cock+knoker","cock+master","cock+mongler","cock+mongruel","cock+monkey","cock+muncher","cock+nose","cock+nugget","cock+sex","cock+shit","cock+smith","cock+smoker","cock+sucker","cockring","colombia plane crash","columbia+crash","condom","condoms","crash+boat","crash+bus","crash+car","crash+ferry","crash+lorry","crash+plane","crash+train","crash+truck","crashed+ combinations","crashes+boat","crashes+bus","crashes+car","crashes+ferry","crashes+lorry","crashes+plane","crashes+train","crashes+truck","cul","cum","cum+bubble","cum+dumpster","cum+guzzler","cum+jockey","cum+slut","cum+tart","cunt","cunt face","cunt hole","cunt licker","cunt rag","cunt slut","cunt+face","cunt+hole","cunt+licker","cunt+rag","cunt+slut","darlene horton","dead+beheading","dead+bomb","dead+crash","dead+disaster","dead+drowned","dead+explosion","dead+genocide","dead+gun","dead+kill","dead+killed","dead+killing","dead+knife","dead+murder","dead+suicide","dead+terrorism","death+beheaded","death+bomb","death+crash","death+drowned","death+explosion","death+gun","death+homicide","death+knife","death+murder","death+suicide","death+terrorism","decapitate","decapitated","decapitation","décès","deep throat","deepthroat","defloration","destruction","détournement","dévastation","devastazione","di origine vulcanica","dick+bag","dick+beaters","dick+face","dick+fuck","dick+fucker","dick+head","dick+hole","dick+juice","dick+milk","dick+monger","dick+slap","dick+sucker","dick+wad","dick+weasel","dick+weed","dick+wod","dieb","diebstahl","dildo","dimitrios xidias","disaster+death","disaster+deaths","disaster+earthquake","disastro","disease+outbreak","distruzione","dogging","domestic+abuse","dominatrix","dood","drecksack","drecksau","drogen","drug+abuse","drug+addict","drug+addiction","drug+death","drug+died","drug+overdose","drugs+dead","drugs+death","drugs+police","dumb ass","dumb fuck","dumb shit","durchsuchung","dyke","dylann roof","earthquake+dead","earthquake+death","earthquake+killed","earthquake+killing","earthquake+kills","ebola","échangistes","edl","ehsanullah ehsan","eier","eierlutscher","einbrecher","einbruch","einen runterholen","eingebrochen","eingestuerzt","eingestürzt","einsturz","einwanderung","einwanderungen","ek521+dubai","emergenza","emirates crash","english defence league","entführen","entführung","entköpfung","erdbeben","erkrankung","erotica","erotika","erschlagen","esame","escort service","esplosione","esplosivo","examen","explosie","explosif","explosion","explosiv","extrem","extremer","extremism","facesitting","fag bag","fag fucker","fag tard","faggit","faggot","faggot cock","faillissement","faillite","fallimento","fat ass","faules miststueck","fellatio","femdom","fesseln","festnahme","fetisch","fick dich","fick mich","ficken","fisting","fkk","flight+crash","flight+crashed","flight+delay","flight+disappeared","flight+missing","flight+vanished","flood+dead","flood+death","flugabsturz","flugschreiber","flugzeugkatastrophe","flugzeugunglück","flugzeugunglueck","foot fetish","fotze","france plane crash","free sex","fritzl","fuck","fuck ass","fuck bag","fuck boy","fuck brain","fuck butt","fuck face","fuck head","fuck hole","fuck in","fuck me","fuck nut","fuck nutt","fuck off","fuck stick","fuck tard","fuck up","fuck wad","fuck wit","fuck witt","fuck you","fucked","fucker","fucker sucker","fuckin","fuckin bitch","fucking","fucking bitch","fucks","fudge packer","fuk me","fuk you","fukin bitch","fummeln","fuuck","gaand","gaandu","gaggers","gala kaatna","gang bang","gangbang","gangguan","gary glitter","gay ass","gay bob","gay bois","gay boys","gay fuckist","gay lord","gay tard","gay wad","gay+abuse","gay+ass","gay+attack","gay+bob","gay+bois","gay+boys","gay+crime","gay+fuckist","gay+lord","gay+sex","gay+tard","gay+wad","gaydo gay fuck","geil","geiler bock","geiles luder","geköpft","gempa bumi","genital warts","georgios chrysikopoulos","germanwings","germanwings+crash","germanwings+crashed","germanwings+crashes","geschlechtsverkehr","gewehr","gilf","gloryhole","goo+crash","group sex","gun+massacre","gun+murder","gun+rampage","gun+shooting","gunshot+fatal","gunung berapi","hacken","handjob","hängetitte","haram","harasment","hardcore sex","heisse","heiβ","heiβe","hentai","hijack","hijack+plane","hijacked+plane","hilaana","hinunterholen","hit and run","holocaust","homo dumbshit","homofuerst","homophobic+attack","hornochse","hospital blast","hostage takers","hubungan luar nikah","huhrensohn","hure","hurenficker","hurengesicht","hurensohn","il terrore","il terrorismo","illegaal","illégal","illegale","imigrasi","imigresen","immigratie","immigraties","immigrazione","immoral","immorale","incidente","indonesia+crash","isis","islamic state","islamist militants","islamophobia","jacques hamel","jamaat-ul-ahrar","jangkitan","janos orsos","jerk off","jet+crash","jihadi","jihadi john","jihadist","jimmy savile","jizz","jo cox","jude","judenvernichtung","junaid jamshed","jungle bunny","junglebunny","junkie","kackbratze","kanone","katastrophe","kaum legal","kebinasaan","kecemasan","keganasan","keller","kemalangan","kematian","kematian-kematian","kemusnahan","khada land","kill+bomb","kill+bombing","kill+death","kill+murder","killed+accident","killed+bomb","killed+bombing","killed+crash","killed+disaster","killed+execution","killed+fatality","killed+knife","killed+murder","killing+attack","killing+bomb","killing+bombing","killing+death","killing+execution","killing+murder","kills+accident","kills+bomb","kills+bombing","kills+crash","kills+disaster","kills+execution","kills+knife","kills+murder","knarre","kokain","kollision","komplot","kondom","konkurs","köpfen","kotak hitam","kotor","krankheit","krankheiten","krebs","kutiyaan","kutti","la catastrofe","la frode","la grippe aviaire","la malattia","la tragedia","le bombe","le malattie","le tragedie","leck mich","letupan","limmigrazione","linfluenza aviaria","linkstraeger","linksträger","lms","lorry attack","lorry+dead","lorry+death","lorry+killed","lorry+kills","lude","luder","lufthansa","lufthansa+crash","lund","lund choosna","lund hilaana","lyon attack","madar chod","maladie","maladies","malapetaka","malattia","malaysia+crash","man decapitated","masalah","masalah-masalah","massacre","massacre+dead","massacre+killed","massacre+killing","massaker","massenvernichtung","masturbation","mengebom","merda","merde","mevlut mert altintas","mh17+crash","mh17+crashed","mh17+malaysia","mh370","mh370+crash","mh370+crashed","mh370+malaysia","michael adebolajo","miefen","mieser stricher","migrant+rape","mile high club","milf","milfs","minge","minibus+died","missbrauch","missgeburt","missonarstellung","miststück","miβbrauch","moerder","mohamed abrini","mohammad daleel","mohammed abdeslam","molest","molestie","mord","mörder","morte","mothafucka","mother fucker","mother fuker","motherfucka","motherfucker","motherfucking","ms804+crash","ms804+egyptair","muft chodna","murdered+dead","murdered+death","murdered+kill","murdered+killed","murdered+killing","murderer+killed","murders+dead","muschi","muschi lecker","mutterficker","nanga naach","national defence league","nazi","neger","nigga","nigger","niggers","niglet","no survivors","noodgeval","notfall","nuage de cendres","nube di cenere","nudes","nudist","nuklear","nuklearwaffen","oasch","oben ohne","omar ismail mostefai","onanieren","onethisch","ongeval","ontplofbaar","onwettig","onzin","opfer","opfern","oplichting","oral sex","orgie","orgy","orsch","osama bin laden","p0rn","paedo and incest websites","paedo+grooming","paedophile","paedophile+grooming","paris shooting","pedophile","pedophilia","pemeriksaan","penangkapan","pengebom","penipuan","penis","penis fucker","penis puffer","penjangkitan","penyakit","perihal gunung berapi","pevers","peverser","pflaume","pidie jaya","piss flaps","pisser","pissnelke","pistole","pk661+crash","pk661+crashed","pk661+crashes","pkk","plane crash","plane crashes","plane+blackbox","plane+crash","plane+crashed","plane+crashes","plane+dead","plane+disaster","plane+diversion","plane+diverted","plane+emergency","plane+explosion","plane+killed","plane+victims","playboy","plunges+balcony","polis","politie","polizei","polizeieinsatz","pondan","poonani","poonany","poontang","porn","porn star","porn stars","porn tube","porno","porno star","pornographic putin","pornos","pornoszene","probleem","pussy licking","puteh manaf","queef","queer bait","queer hole","raand","racism","racist","rakete","rampas","rape","rape+assault","rape+assaults","raped","raper","rapes","raping","rapist","rassismus","rassist","rassistisch","redtube","reina+istanbul","rettung","rettungseinsatz","rim job","rim jobs","rimborso","rimjob","rolf harris","rotlicht","rückerstattung","runterholen","sadomaso","saeufer","sakit","salah abdeslam","salmonela","salmonella","salmonelle","salmonellen","samalaingik","sand nigger","sandnigger","sau","saufen","säufer","saunauntersitzer","scambisti","scandale","scandalo","scatola nera","schandaal","scharf","scharfe","scheide","scheise","scheiße","scheissegal","scheissen","scheisser","scheissfreundlich","scheissgesicht","scheisskerl","scheisskopf","scheiβe","schiesserei","schieβerei","schlampe","schmutzig","schnepfe","schockiert","schrecklich","schrullig","schuetze","schütze","schwanz","schwanz lecker","schwanz lutschen","schwanzlutscher","schweinepriest","schwindel","schwuchtel","schwul","schwuler","seks","selbstbefriedigung","selesema burung","senjata","serang","sesso","severely injured","sex chat","sex club","sex dvd","sex shop","sex shops","sex sites","sex toys","sex video","sex videos","sex+abuse","sex+assault","sex+attack","sex+blowjob","sex+child","sex+cum","sex+dick","sex+dildo","sex+dildos","sex+erotic","sex+grooming","sex+hardcore","sex+incest","sex+kamasutra","sex+masturbation","sex+orgy","sex+paedophile","sex+porn","sex+porno","sex+pussy","sex+rape","sex+raped","sex+trafficking","sex+upskirt","sex+victim","sex+victims","sex+whore","sex+xxx","sexaktiv","sexe","sexismus","sexist","sexshop","sexstellung","sexual+abuse","sexual+assault","sexual+grooming","sexual+paedo","sexual+paedophile","sexualität","sexvideo","sexxx","shabdkosh","shav kamukata","shishkoff","shit","shit ass","shit bag","shit bagger","shit brains","shit breath","shit cunt","shit dick","shit face","shit faced","shit head","shit hole","shit house","shit spitter","shit stain","shit+ass","shit+bag","shit+bagger","shit+brains","shit+breath","shit+cunt","shit+dick","shit+face","shit+faced","shit+head","shit+hole","shit+house","shit+spitter","shooting+homicide","sinai crash","sitzpinkler","skandal","skull fuck","slut","slutbag","sluts","sodomy","softcore","spermizid","spital","sporco","stabbed+fatal","ständer","starb","starben","sterben","sterfgevallen","stirbt","strap on","strapon","strich","stricherin","strichjunge","strichmädchen","strippen","striptease","suicide bomber","suicide+bomb","suicide+bomber","swinger","swinger club","swingers","taliban","tatang sulaiman","teensex","terorisme","terremoto","terreur","terror","terror attack","terror+attack","terrorism","terrorisme","terrorismus","terrorist","terrorist attack","terrorists","terugbetaling","the gambia","thokna","throat slit","tidak beretika","tit anime","tit fuck","titfuck","titte","titten","tittyfuck","tk6491","tod","todesfälle","todesopfer","todesschuetze","tot","tragedi","tragedi-tragedi","tragédie","tragédies","tragico","tragik","tragique","tragisch","tragödie","tragödien","train crash","transvestit","transvestiten","trauern","travesti","travestiet","travestis","travestiti","travestito","tremblement de terre","truck+dead","truck+death","truck+killed","truck+kills","truffa","trümmer","tsunami","tsunami+disaster","tunisia attack","tussi","twat","twat waffle","twatlips","twats","überdosis","überfall","überfallen","udara","ueberfall","ueberfallen","uncle fucker","underage girls","unethisch","unfall","unglück","unglueck","untergegangen","upskirt","upskirts","urgence","verdächtige","verdammt","verfickt","vergammelt","vergammeltes","vergewaltiger","vergewaltigte","vergewaltigung","verhurt","verhurtes","vermisste","vernichtung","vernietiging","verpiss dich","verseuchen","verseuchung","versunken","verwoesting","verwüstung","vibrator","vimaan visphot","viren","virus","virus-virus","virussen","visa-visa","visen","visphot","visti","visto","visum","voegeln","vogelgriep","vogelgrippe","vögeln","volcan","volcanique","völkermord","von hinten","vorwärtseinparker","vuil","vulcano","vulkaan","vulkan","vulkanasche","vulkanisch","vulkanische as","waffe","waffen","wank","wanking","wapen","war+aleppo","war+ankara","war+bomb","war+bombing","war+crime","war+dead","war+death","war+deaths","war+explosion","war+isis","war+killed","war+murder","war+syria","war+victim","war+victims","warmduscher","webcam","white nationalism","white separatism","whore","whorebag","whoreface","wichser","wixer","x -rated","x-rated","xstrim","xtreme","xxxrated","zakaria bulhan","zerstoerung","zerstörung","zicke","ziekte","ziekten","zoosex","zusammensturz","zwarte doos","ウイルス","ウェブカメラ","エクストリーム","がらくた","サルモネラ菌","スウィンガーズ","スキャンダル","セックス","テロ","ハイジャック","ビザ","ブラックボックス","ほぼ合法","不雅視頻通常有關名人","不雅视频","严重腹泻","事件","事故","交换配偶性交","交換配偶性交","他妈的","免费色情","全裸亲热","全裸親熱","出軌","出轨","口交","向女人脸上射精","向女人臉上射精","吸毒","吸毒者","問題","嚴重腹瀉","地狱","地獄","地震","处女膜","大变态","大變態","大阴精","大陰精","女同","女同性恋","女装","好色之徒","嫌がらせ","尻","屁眼痛","崩盘","废物","廢物","异装癖","性","性游戏","性病","性遊戲","性騷擾","性骚扰","性高潮","恋童癖","恐怖","恐怖主义","恐怖主義","恐怖袭击","悲劇","悲劇的な","意外","懒妇","成人向け","手淫","打手枪","打手槍","打野战","打野戰","换位","换性者","換位","搖頭丸","摇头丸","操","攻击","攻撃","攻擊","无限制","早泄","早洩","易装癖者","易裝癖者","有性","机毁人亡","极端分子","検査","武器","死","死亡","毒品","汚い","津波","流氓头","流氓頭","火山","火山灰","火山灰云","火山灰雲","灰の雲","災害","炸弹","無限制","爆弾","爆撃","爆撃機","爆発","狗屎","男同","異裝癖","病気","白痴","白癡","破产","破壊","破產","破産","移民","空中","紅顏禍水","緊急","红颜祸水","肛管","肛门","肥婆","胡搞","色情","荒廃","荡妇","蕩婦","處女膜","蠢驢","蠢驴","裸聊","詐欺","警察","跟踪狂","跟蹤狂","轰炸","返金","违法边缘","逮捕","逼","違法","違法邊緣","避孕套","阳痿","阴吹","阴道","限制級","限制级","陰吹","陽痿","雞雞","非主流","非倫理的","鳥インフルエンザ","鸡鸡","갈보","게이","구강 성교","그룹 섹스","나쁜년","도색물","동성애자","레즈비언","마약","보지","북한","사기","성교","성기","성인 용품","섹스","섹스 토이","셉스샵","애널","야동","엉덩이","에로틱","오랄 섹스","오랄섹스","자위","자유 섹스","자지","집단 성교","창녀","콘돔","콜걸","페니스","포르노","포르노 스타","폭탄","항공기 충돌","항문"];
var message = "//data12.adlooxtracking.com/ads/ic2.php?fw=1&iframe=3&version=2&client=infectious&banniere=banoneinf&id_editeur=WKNUE-FkCAotNcf8_ADLOOX_ID_58999_ADLOOX_ID_728x90_ADLOOX_ID_302_ADLOOX_ID_50495_ADLOOX_ID_337184_ADLOOX_ID_openx_83499_ADLOOX_ID__ADLOOX_ID_578045_ADLOOX_ID_deu&campagne=infectiousg&methode=%3B&fai=ox_9518377573_536491%40http%3A%2F%2Fwww.101cookbooks.com%2Farchives%2Fsummer-berry-crisp-recipe.html&url_referrer=http%3A%2F%2Fwww.101cookbooks.com%2Farchives%2Fsummer-berry-crisp-recipe.html&ads_forceblock=1&log=1&visite_id=55706645987";
getAllNodesContent ( firstNode, contentTab, message );
var adloox_impression=1;