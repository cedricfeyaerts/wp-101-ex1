jQuery(document).ready(function($){
	var titleH = $("h1.fn").text();
	var metaD = $('meta[name="description"]').attr("content");
	var pinD = titleH + " - " + metaD + " - from 101Cookbooks.com";
    $('.entrybody img').each(function(){
    $(this).attr('data-pin-description', pinD);
  });        
         
 });

 $(function(){
    $(window).resize(function() {    
        processBodies($(window).width());
    });
    function processBodies(width) {
        if(width > 768) {
            $('#commentdisplay').collapse('show');
        }
        else {
            $('#commentdisplay').collapse('hide');
        }
    }
    processBodies($(window).width());
}); 