<!DOCTYPE html>
<html lang="en" class="wf-proximanova1-n4-active wf-proximanova1-i4-active wf-chaparralpro1-n4-active wf-chaparralpro1-i4-active wf-active"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Appetizer Recipes - 101 Cookbooks</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 




<meta name="description" content="Looking for great appetizer recipes? These are the best appetizer recipes from the award-winning 101 Cookbooks recipe journal.">
<meta name="author" content="Heidi Swanson">
<meta name="Copyright" content="Copyright (c) 2000-2016 Heidi Swanson">
<meta name="keywords" content="Appetizer Recipes, healthy recipes, natural foods, cookbooks, food blog">
<meta property="og:site_name" content="101 Cookbooks">
<meta property="og:description" content="Looking for great appetizer recipes? These are the best appetizer recipes from the award-winning 101 Cookbooks recipe journal.">
 <meta property="og:title" content="Appetizer Recipes">
<meta property="twitter:site" content="@101cookbooks">
      <meta property="twitter:creator" content="@101cookbooks">
      <meta property="twitter:title" content="Appetizer Recipes recipes">
        <meta property="twitter:domain" content="http://www.101cookbooks.com">
      <meta property="twitter:description" content="Looking for great appetizer recipes? These are the best appetizer recipes from the award-winning 101 Cookbooks recipe journal.">


 
 
 
<link rel="alternate" type="application/rss+xml" title="101 Cookbooks" href="http://feeds.101cookbooks.com/101cookbooks">
<script async="" src="<?php echo get_template_directory_uri() ?>/assets/async-ads.js"></script><script async="" src="<?php echo get_template_directory_uri() ?>/assets/async-ads.js"></script><script src="<?php echo get_template_directory_uri() ?>/assets/osd.js"></script><script src="<?php echo get_template_directory_uri() ?>/assets/ads"></script><script async="" src="<?php echo get_template_directory_uri() ?>/assets/beacon.js"></script><script async="" type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/gpt.js"></script><script type="text/javascript" async="" src="<?php echo get_template_directory_uri() ?>/assets/cse.js"></script><script type="text/javascript" async="" src="<?php echo get_template_directory_uri() ?>/assets/ga.js"></script><script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/jny5sda.js"></script>
<style type="text/css">.tk-proxima-nova{font-family:"proxima-nova-1","proxima-nova-2",sans-serif;}.tk-chaparral-pro{font-family:"chaparral-pro-1","chaparral-pro-2",sans-serif;}</style><style type="text/css">@font-face{font-family:proxima-nova-1;src:url(https://use.typekit.com/af/04fb9d/00000000000000003b9ad1b9/27/l?subset_id=2&fvd=n4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("woff2"),url(https://use.typekit.com/af/04fb9d/00000000000000003b9ad1b9/27/d?subset_id=2&fvd=n4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("woff"),url(https://use.typekit.com/af/04fb9d/00000000000000003b9ad1b9/27/a?subset_id=2&fvd=n4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("opentype");font-weight:400;font-style:normal;}@font-face{font-family:proxima-nova-1;src:url(https://use.typekit.com/af/8f1252/00000000000000003b9ad1ba/27/l?subset_id=2&fvd=i4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("woff2"),url(https://use.typekit.com/af/8f1252/00000000000000003b9ad1ba/27/d?subset_id=2&fvd=i4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("woff"),url(https://use.typekit.com/af/8f1252/00000000000000003b9ad1ba/27/a?subset_id=2&fvd=i4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("opentype");font-weight:400;font-style:italic;}@font-face{font-family:chaparral-pro-1;src:url(https://use.typekit.com/af/95d314/000000000000000000012dac/27/l?subset_id=2&fvd=n4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("woff2"),url(https://use.typekit.com/af/95d314/000000000000000000012dac/27/d?subset_id=2&fvd=n4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("woff"),url(https://use.typekit.com/af/95d314/000000000000000000012dac/27/a?subset_id=2&fvd=n4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("opentype");font-weight:400;font-style:normal;}@font-face{font-family:chaparral-pro-1;src:url(https://use.typekit.com/af/b1f64b/000000000000000000012da0/27/l?subset_id=2&fvd=i4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("woff2"),url(https://use.typekit.com/af/b1f64b/000000000000000000012da0/27/d?subset_id=2&fvd=i4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("woff"),url(https://use.typekit.com/af/b1f64b/000000000000000000012da0/27/a?subset_id=2&fvd=i4&token=uIqNsM%2FsKN2mowNLitPFnAsnKha00jMXoxPJeNkRUu3X7yEBSLPXKovMnVA6syAW) format("opentype");font-weight:400;font-style:italic;}</style><script type="text/javascript">try{Typekit.load();}catch(e){}</script>





 
<script type="text/javascript">
 $(document).ready(function() {
$('div.thumbholder').mouseenter(function(){
      $(this).find('.thumb').show(30);
    }).mouseleave(function(){
        $(this).find('.thumb').hide(30);
    });
    });
    </script>
 


<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/bootstrap-theme.min.css">
<script src="<?php echo get_template_directory_uri() ?>/assets/bootstrap.min.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/styles.2015.css" type="text/css">

  
<script src="<?php echo get_template_directory_uri() ?>/assets/pubads_impl_110.js" async=""></script><link rel="prefetch" href="http://tpc.googlesyndication.com/safeframe/1-0-6/html/container.html"><script src="<?php echo get_template_directory_uri() ?>/assets/jsapi" type="text/javascript"></script><link type="text/css" href="<?php echo get_template_directory_uri() ?>/assets/default+en.css" rel="stylesheet"><link type="text/css" href="<?php echo get_template_directory_uri() ?>/assets/default.css" rel="stylesheet"><script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/default+en.I.js"></script><style type="text/css">
.gsc-control-cse {
font-family: Arial, sans-serif;
border-color: #FFFFFF;
background-color: #FFFFFF;
}
.gsc-control-cse .gsc-table-result {
font-family: Arial, sans-serif;
}
input.gsc-input {
border-color: #BCCDF0;
}
input.gsc-search-button {
border-color: #666666;
background-color: #ffffff;
}
.gsc-tabHeader.gsc-tabhInactive {
border-color: #E9E9E9;
background-color: #E9E9E9;
}
.gsc-tabHeader.gsc-tabhActive {
border-top-color: #FF9900;
border-left-color: #E9E9E9;
border-right-color: #E9E9E9;
background-color: #FFFFFF;
}
.gsc-tabsArea {
border-color: #E9E9E9;
}
.gsc-webResult.gsc-result,
.gsc-results .gsc-imageResult {
border-color: #FFFFFF;
background-color: #FFFFFF;
}
.gsc-webResult.gsc-result:hover,
.gsc-imageResult:hover {
border-color: #FFFFFF;
background-color: #FFFFFF;
}
.gsc-webResult.gsc-result.gsc-promotion:hover {
border-color: #FFFFFF;
background-color: #FFFFFF;
}
.gs-webResult.gs-result a.gs-title:link,
.gs-webResult.gs-result a.gs-title:link b,
.gs-imageResult a.gs-title:link,
.gs-imageResult a.gs-title:link b {
color: #666666;
}
.gs-webResult.gs-result a.gs-title:visited,
.gs-webResult.gs-result a.gs-title:visited b,
.gs-imageResult a.gs-title:visited,
.gs-imageResult a.gs-title:visited b {
color: #AF9277;
}
.gs-webResult.gs-result a.gs-title:hover,
.gs-webResult.gs-result a.gs-title:hover b,
.gs-imageResult a.gs-title:hover,
.gs-imageResult a.gs-title:hover b {
color: #9F6C3C;
}
.gs-webResult.gs-result a.gs-title:active,
.gs-webResult.gs-result a.gs-title:active b,
.gs-imageResult a.gs-title:active,
.gs-imageResult a.gs-title:active b {
color: #666666;
}
.gsc-cursor-page {
color: #666666;
}
a.gsc-trailing-more-results:link {
color: #666666;
}
.gs-webResult .gs-snippet,
.gs-imageResult .gs-snippet,
.gs-fileFormatType {
color: #666666;
}
.gs-webResult div.gs-visibleUrl,
.gs-imageResult div.gs-visibleUrl {
color: #333333;
}
.gs-webResult div.gs-visibleUrl-short {
color: #333333;
}
.gs-webResult div.gs-visibleUrl-short {
display: block;
}
.gs-webResult div.gs-visibleUrl-long {
display: none;
}
.gs-promotion div.gs-visibleUrl-short {
display: none;
}
.gs-promotion div.gs-visibleUrl-long {
display: block;
}
.gsc-cursor-box {
border-color: #FFFFFF;
}
.gsc-results .gsc-cursor-box .gsc-cursor-page {
border-color: #E9E9E9;
background-color: #FFFFFF;
color: #666666;
}
.gsc-results .gsc-cursor-box .gsc-cursor-current-page {
border-color: #FF9900;
background-color: #FFFFFF;
color: #AF9277;
}
.gsc-webResult.gsc-result.gsc-promotion {
border-color: #336699;
background-color: #FFFFFF;
}
.gsc-completion-title {
color: #666666;
}
.gsc-completion-snippet {
color: #666666;
}
.gs-promotion a.gs-title:link,
.gs-promotion a.gs-title:link *,
.gs-promotion .gs-snippet a:link {
color: #0000CC;
}
.gs-promotion a.gs-title:visited,
.gs-promotion a.gs-title:visited *,
.gs-promotion .gs-snippet a:visited {
color: #0000CC;
}
.gs-promotion a.gs-title:hover,
.gs-promotion a.gs-title:hover *,
.gs-promotion .gs-snippet a:hover {
color: #0000CC;
}
.gs-promotion a.gs-title:active,
.gs-promotion a.gs-title:active *,
.gs-promotion .gs-snippet a:active {
color: #0000CC;
}
.gs-promotion .gs-snippet,
.gs-promotion .gs-title .gs-promotion-title-right,
.gs-promotion .gs-title .gs-promotion-title-right * {
color: #000000;
}
.gs-promotion .gs-visibleUrl,
.gs-promotion .gs-visibleUrl-short {
color: #008000;
}</style><style type="text/css">.gscb_a{display:inline-block;font:27px/13px arial,sans-serif}.gsst_a .gscb_a{color:#a1b9ed;cursor:pointer}.gsst_a:hover .gscb_a,.gsst_a:focus .gscb_a{color:#36c}.gsst_a{display:inline-block}.gsst_a{cursor:pointer;padding:0 4px}.gsst_a:hover{text-decoration:none!important}.gsst_b{font-size:16px;padding:0 2px;position:relative;user-select:none;-webkit-user-select:none;white-space:nowrap}.gsst_e{opacity:0.55;}.gsst_a:hover .gsst_e,.gsst_a:focus .gsst_e{opacity:0.72;}.gsst_a:active .gsst_e{opacity:1;}.gsst_f{background:white;text-align:left}.gsst_g{background-color:white;border:1px solid #ccc;border-top-color:#d9d9d9;box-shadow:0 2px 4px rgba(0,0,0,0.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);margin:-1px -3px;padding:0 6px}.gsst_h{background-color:white;height:1px;margin-bottom:-1px;position:relative;top:-1px}.gsib_a{width:100%;padding:4px 6px 0}.gsib_a,.gsib_b{vertical-align:top}.gssb_c{border:0;position:absolute;z-index:989}.gssb_e{border:1px solid #ccc;border-top-color:#d9d9d9;box-shadow:0 2px 4px rgba(0,0,0,0.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);cursor:default}.gssb_f{visibility:hidden;white-space:nowrap}.gssb_k{border:0;display:block;position:absolute;top:0;z-index:988}.gsdd_a{border:none!important}.gsq_a{padding:0}.gscsep_a{display:none}.gssb_a{padding:0 7px}.gssb_a,.gssb_a td{white-space:nowrap;overflow:hidden;line-height:22px}#gssb_b{font-size:11px;color:#36c;text-decoration:none}#gssb_b:hover{font-size:11px;color:#36c;text-decoration:underline}.gssb_g{text-align:center;padding:8px 0 7px;position:relative}.gssb_h{font-size:15px;height:28px;margin:0.2em;-webkit-appearance:button}.gssb_i{background:#eee}.gss_ifl{visibility:hidden;padding-left:5px}.gssb_i .gss_ifl{visibility:visible}a.gssb_j{font-size:13px;color:#36c;text-decoration:none;line-height:100%}a.gssb_j:hover{text-decoration:underline}.gssb_l{height:1px;background-color:#e5e5e5}.gssb_m{color:#000;background:#fff}.gsfe_a{border:1px solid #b9b9b9;border-top-color:#a0a0a0;box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);-moz-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);-webkit-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);}.gsfe_b{border:1px solid #4d90fe;outline:none;box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);-moz-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);-webkit-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);}.gssb_a{padding:0 7px}.gssb_e{border:0}.gssb_l{margin:5px 0}.gssb_c .gsc-completion-container{position:static}.gssb_c{z-index:5000}.gsc-completion-container table{background:transparent;font-size:inherit;font-family:inherit}.gssb_c > tbody > tr,.gssb_c > tbody > tr > td,.gssb_d,.gssb_d > tbody > tr,.gssb_d > tbody > tr > td,.gssb_e,.gssb_e > tbody > tr,.gssb_e > tbody > tr > td{padding:0;margin:0;border:0}.gssb_a table,.gssb_a table tr,.gssb_a table tr td{padding:0;margin:0;border:0}</style><style type="text/css">.gscb_a{display:inline-block;font:27px/13px arial,sans-serif}.gsst_a .gscb_a{color:#a1b9ed;cursor:pointer}.gsst_a:hover .gscb_a,.gsst_a:focus .gscb_a{color:#36c}.gsst_a{display:inline-block}.gsst_a{cursor:pointer;padding:0 4px}.gsst_a:hover{text-decoration:none!important}.gsst_b{font-size:16px;padding:0 2px;position:relative;user-select:none;-webkit-user-select:none;white-space:nowrap}.gsst_e{opacity:0.55;}.gsst_a:hover .gsst_e,.gsst_a:focus .gsst_e{opacity:0.72;}.gsst_a:active .gsst_e{opacity:1;}.gsst_f{background:white;text-align:left}.gsst_g{background-color:white;border:1px solid #ccc;border-top-color:#d9d9d9;box-shadow:0 2px 4px rgba(0,0,0,0.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);margin:-1px -3px;padding:0 6px}.gsst_h{background-color:white;height:1px;margin-bottom:-1px;position:relative;top:-1px}.gsib_a{width:100%;padding:4px 6px 0}.gsib_a,.gsib_b{vertical-align:top}.gssb_c{border:0;position:absolute;z-index:989}.gssb_e{border:1px solid #ccc;border-top-color:#d9d9d9;box-shadow:0 2px 4px rgba(0,0,0,0.2);-webkit-box-shadow:0 2px 4px rgba(0,0,0,0.2);cursor:default}.gssb_f{visibility:hidden;white-space:nowrap}.gssb_k{border:0;display:block;position:absolute;top:0;z-index:988}.gsdd_a{border:none!important}.gsq_a{padding:0}.gscsep_a{display:none}.gssb_a{padding:0 7px}.gssb_a,.gssb_a td{white-space:nowrap;overflow:hidden;line-height:22px}#gssb_b{font-size:11px;color:#36c;text-decoration:none}#gssb_b:hover{font-size:11px;color:#36c;text-decoration:underline}.gssb_g{text-align:center;padding:8px 0 7px;position:relative}.gssb_h{font-size:15px;height:28px;margin:0.2em;-webkit-appearance:button}.gssb_i{background:#eee}.gss_ifl{visibility:hidden;padding-left:5px}.gssb_i .gss_ifl{visibility:visible}a.gssb_j{font-size:13px;color:#36c;text-decoration:none;line-height:100%}a.gssb_j:hover{text-decoration:underline}.gssb_l{height:1px;background-color:#e5e5e5}.gssb_m{color:#000;background:#fff}.gsfe_a{border:1px solid #b9b9b9;border-top-color:#a0a0a0;box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);-moz-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);-webkit-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.1);}.gsfe_b{border:1px solid #4d90fe;outline:none;box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);-moz-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);-webkit-box-shadow:inset 0px 1px 2px rgba(0,0,0,0.3);}.gssb_a{padding:0 7px}.gssb_e{border:0}.gssb_l{margin:5px 0}.gssb_c .gsc-completion-container{position:static}.gssb_c{z-index:5000}.gsc-completion-container table{background:transparent;font-size:inherit;font-family:inherit}.gssb_c > tbody > tr,.gssb_c > tbody > tr > td,.gssb_d,.gssb_d > tbody > tr,.gssb_d > tbody > tr > td,.gssb_e,.gssb_e > tbody > tr,.gssb_e > tbody > tr > td{padding:0;margin:0;border:0}.gssb_a table,.gssb_a table tr,.gssb_a table tr td{padding:0;margin:0;border:0}</style></head>
<body>
<div class="container-fluid" style="visibility: visible; opacity: 1;">
<header class="hidden-xs">
<div class="row"><div id="top-logo" class="col-sm-4 col-sm-offset-3 col-md-offset-2 col-lg-offset-4 col-md-3 col-lg-2"><a href="http://www.101cookbooks.com/" title="101 Cookbooks" rel="home"><img src="<?php echo get_template_directory_uri() ?>/assets/101-cookbooks-logo.png" alt="101 Cookbooks" border="0" height="40" width="201"></a><!-- = end top logo = --></div>
<div class="searchform col-sm-4 col-md-3 col-lg-3 col-lg-offset-2   pull-right"><!-- SiteSearch Google --><div id="___gcse_0"><div class="gsc-control-searchbox-only gsc-control-searchbox-only-en" dir="ltr"><form class="gsc-search-box" accept-charset="utf-8"><table cellspacing="0" cellpadding="0" class="gsc-search-box"><tbody><tr><td class="gsc-input"><input autocomplete="off" type="text" size="10" class=" gsc-input " name="search" title="search" id="gsc-i-id1" dir="ltr" spellcheck="false" style="outline: none; background: url(&quot;http://www.google.com/cse/static/en/google_custom_search_watermark.gif&quot;) left center no-repeat rgb(255, 255, 255);"><input type="hidden" name="bgresponse" id="bgresponse"></td><td class="gsc-search-button"><input type="button" value="Search" class="gsc-search-button" title="search"></td><td class="gsc-clear-button"><div class="gsc-clear-button" title="clear results">&nbsp;</div></td></tr></tbody></table><table cellspacing="0" cellpadding="0" class="gsc-branding"><tbody><tr><td class="gsc-branding-user-defined"></td><td class="gsc-branding-text"><div class="gsc-branding-text">powered by</div></td><td class="gsc-branding-img"><img src="<?php echo get_template_directory_uri() ?>/assets/small-logo.png" class="gsc-branding-img"></td></tr></tbody></table></form></div></div><!-- SiteSearch Google --><!-- = end searchform = --></div></div>
</header>	<!--/ header -->			
<!-- == begin nav column == -->
<div class="navcol col-sm-3 col-lg-2 navbar-fixed-top sidebar-nav">
<div class="container navcontainer">
<div class="navbar navbar-default" role="navigation">
 <div class="navbar-header">
        <a href="http://www.101cookbooks.com/" title="101 Cookbooks" rel="home"><img src="<?php echo get_template_directory_uri() ?>/assets/101-cookbooks-logo.png" alt="101 Cookbooks" border="0" class="moblogo hidden-sm hidden-md hidden-lg"></a> <a href="http://www.101cookbooks.com/appetizers/#menu-toggle" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">More Recipes <span class="caret"></span></a>
 </div><!--/ navbar-header -->
        <div class="navbar-collapse collapse sidebar-navbar-collapse">  
          <ul class="nav navbar-nav">
	            <li class="shop"><a href="http://www.quitokeeto.com/" onclick="_gaq.push([&#39;_trackEvent&#39;, &#39;QKhousead&#39;, &#39;leftcol&#39;, &#39;navitem&#39;]);">Shop</a></li>	    
      <li class="dropdown">
          <a href="http://www.101cookbooks.com/appetizers/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Categories <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu"><li><a href="http://www.101cookbooks.com/breakfast_brunch/">Breakfast</a></li><li><a href="http://www.101cookbooks.com/quick_recipes/">Quick</a></li><li><a href="http://www.101cookbooks.com/salads/">Salads</a></li><li><a href="http://www.101cookbooks.com/soups/">Soups</a></li><li><a href="http://www.101cookbooks.com/vegetarian_recipes/">Vegetarian</a></li><li><a href="http://www.101cookbooks.com/vegan_recipes/">Vegan </a></li> <li><a href="http://www.101cookbooks.com/whole_grain_recipes/">Whole Grain</a></li> <li class="recipecats"><a href="http://www.101cookbooks.com/appetizers/">Appetizer </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/baked_goods/">Baked Goods </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/basic_techniques/">Basic Techniques</a></li><li class="recipecats"><a href="http://www.101cookbooks.com/chocolate_recipes/">Chocolate </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/cookies/">Cookie </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/desserts/">Dessert </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/drink_recipes/">Drink </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/gluten_free_recipes/">Gluten Free </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/heidis_favorites/">Heidi's Favorites</a></li><li class="recipecats"><a href="http://www.101cookbooks.com/high_protein_recipes/">High Protein </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/holiday_recipes/">Holiday </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/low_carb_recipes/">Low Carb </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/main_courses/">Main Course </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/pies_and_tarts/">Pies and Tart </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/sandwiches/">Sandwich </a></li><li class="recipecats"><a href="http://www.101cookbooks.com/sides/">Side Dish </a></li>         
          </ul>
        </li>
 
            <li class="dropdown">
          <a href="http://www.101cookbooks.com/appetizers/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Ingredients <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu"><li><a href="http://www.101cookbooks.com/ingredient.html"><em>All</em></a></li>
	          <li><a href="http://www.101cookbooks.com/ingredient/avocado">Avocado</a></li><li><a href="http://www.101cookbooks.com/ingredient/egg">Egg</a></li>
<li><a href="http://www.101cookbooks.com/ingredient/herb">Herbs</a></li>  <li><a href="http://www.101cookbooks.com/ingredient/kale">Kale</a></li>
  <li><a href="http://www.101cookbooks.com/ingredient/lemon">Lemon</a></li>    <li><a href="http://www.101cookbooks.com/ingredient/lentil">Lentil</a></li>
  <li><a href="http://www.101cookbooks.com/ingredient/quinoa">Quinoa</a></li>  <li><a href="http://www.101cookbooks.com/ingredient/pasta">Pasta</a></li>
  <li><a href="http://www.101cookbooks.com/ingredient/tomato">Tomato</a></li>  <li><a href="http://www.101cookbooks.com/ingredient/yogurt">Yogurt</a></li>
  <li><a href="http://www.101cookbooks.com/ingredient/zucchini">Zucchini</a></li>     <li><a href="http://www.101cookbooks.com/ingredient/arugula">Arugula</a></li> 
   <li><a href="http://www.101cookbooks.com/ingredient/asparagus">Asparagus</a></li><li class="ingred"><a href="http://www.101cookbooks.com/ingredient/basil">Basil</a></li>
   <li class="ingred"><a href="http://www.101cookbooks.com/ingredient/broccoli">Broccoli</a></li> <li class="ingred"><a href="http://www.101cookbooks.com/ingredient/buttermilk">Buttermilk</a></li>    <li class="ingred"><a href="http://www.101cookbooks.com/ingredient/cauliflower">Cauliflower</a></li><li class="ingred"><a href="http://www.101cookbooks.com/ingredient/chickpea">Chickpeas</a></li>   <li class="ingred"><a href="http://www.101cookbooks.com/ingredient/chocolate">Chocolate</a></li> <li class="ingred"><a href="http://www.101cookbooks.com/ingredient/curry">Curry</a></li>   <li class="ingred"><a href="http://www.101cookbooks.com/ingredient/tempeh">Tempeh</a></li> <li class="ingred"><a href="http://www.101cookbooks.com/ingredient/tofu">Tofu</a></li>   </ul>
        </li>
       <li class="dropdown">
          <a href="http://www.101cookbooks.com/appetizers/#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">By Season <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
	 <li><a href="http://www.101cookbooks.com/spring">Spring</a></li>
   <li><a href="http://www.101cookbooks.com/summer">Summer</a></li>
   <li><a href="http://www.101cookbooks.com/fall">Fall</a></li>
   <li><a href="http://www.101cookbooks.com/winter">Winter</a></li>
 </ul>
  </li>
  <li class="dropdown">
          <a href="http://www.101cookbooks.com/appetizers/#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="font-size:.9em;">Recommended Cookbooks<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu"> 
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/baking-cookbooks.html">Baking Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/california-cookbooks.html">California Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/classic-cookbooks.html">Classic Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/general-cookbooks.html">General Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/indian-cookbooks.html">Indian Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/italian-cookbooks.html">Italian Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/japanese-cookbooks.html">Japanese Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/middle-eastern-cookbooks.html">Middle Eastern Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/natural-food-cookbooks.html">Natural Food Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/reference-cookbooks.html">Reference Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/restaurant-cookbooks.html">Restaurant Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/scandinavian-cookbooks.html">Scandinavian Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/vegan-cookbooks.html">Vegan Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/vegetarian-cookbooks.html">Vegetarian Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/weeknight-cookbooks.html">Weeknight Cookbooks</a></li>
   <li><a href="http://www.101cookbooks.com/recommended-cookbooks/wellness-cookbooks.html">Wellness Cookbooks</a></li>
 </ul>
  </li>
   <li class="dropdown">
          <a href="http://www.101cookbooks.com/appetizers/#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="font-size:.9em;">Natural Foods Basics<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
	 <li><a href="http://www.101cookbooks.com/archives/cooking-with-natural-foods-recipe.html">Cooking with Natural Foods</a></li>
   <li><a href="http://www.101cookbooks.com/archives/a-few-favorite-fats-and-oils-recipe.html">Favorite Fats &amp; Oils</a></li>
   <li><a href="http://www.101cookbooks.com/archives/a-few-favorite-sweeteners-recipe.html">Favorite Sweeteners</a></li>
   <li><a href="http://www.101cookbooks.com/archives/a-few-favorite-whole-grains-recipe.html">Favorite Whole Grains</a></li>
   <li><a href="http://www.101cookbooks.com/archives/more-favorite-ingredients-recipe.html">Great Whole Foods</a></li>
 </ul>
  </li>
  
  <li><a href="http://www.101cookbooks.com/archives.html">Recipe Index</a></li>
    </ul>
          
          <div class="searchform col-xs-10  hidden-lg hidden-sm hidden-md"><!-- SiteSearch Google --><div id="___gcse_1"><div class="gsc-control-searchbox-only gsc-control-searchbox-only-en" dir="ltr"><form class="gsc-search-box" accept-charset="utf-8"><table cellspacing="0" cellpadding="0" class="gsc-search-box"><tbody><tr><td class="gsc-input"><input autocomplete="off" type="text" size="10" class=" gsc-input" name="search" title="search" id="gsc-i-id2" dir="ltr" spellcheck="false" style="outline: none; background: url(&quot;http://www.google.com/cse/static/en/google_custom_search_watermark.gif&quot;) left center no-repeat rgb(255, 255, 255);"><input type="hidden" name="bgresponse" id="bgresponse"></td><td class="gsc-search-button"><input type="button" value="Search" class="gsc-search-button" title="search"></td><td class="gsc-clear-button"><div class="gsc-clear-button" title="clear results">&nbsp;</div></td></tr></tbody></table><table cellspacing="0" cellpadding="0" class="gsc-branding"><tbody><tr><td class="gsc-branding-user-defined"></td><td class="gsc-branding-text"><div class="gsc-branding-text">powered by</div></td><td class="gsc-branding-img"><img src="<?php echo get_template_directory_uri() ?>/assets/small-logo.png" class="gsc-branding-img"></td></tr></tbody></table></form></div></div><!-- SiteSearch Google --><!-- = end searchform = --></div> 
          
          <div class="socialicons"><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;topbar&#39;,&#39;twitter&#39;]);" href="https://twitter.com/#!/101Cookbooks"><img src="<?php echo get_template_directory_uri() ?>/assets/twitter.png" alt="connect on twitter" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;topbar&#39;,&#39;facebook&#39;]);" href="http://www.facebook.com/101cookbooks"><img src="<?php echo get_template_directory_uri() ?>/assets/facebook.png" alt="connect on facebook" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;topbar&#39;,&#39;instragram&#39;]);" href="http://instagram.com/heidijswanson/"><img src="<?php echo get_template_directory_uri() ?>/assets/instagram.png" alt="connect on instagram" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;topbar&#39;,&#39;pinterest&#39;]);" href="http://pinterest.com/heidiswanson/"><img src="<?php echo get_template_directory_uri() ?>/assets/pinterest.png" alt="connect on pinterest" width="20" height="20"></a></div><!--/ socialicons -->

</div><!--/nav-collapse --> 
</div>  <!--/navbar navbar-default -->
 <br class="clear">
</div> <!--/ navcontainer -->
</div>	<!--/navcol -->	
<!-- == begin main content column == -->
<div id="maincontent" class="col-sm-9 col-lg-7" style="visibility: visible; opacity: 1;">
 	  <h1 id="astitle">Appetizer Recipes</h1>








<div class="pinheader" style="display:none;"><img src="<?php echo get_template_directory_uri() ?>/assets/indian-spiced-guacamole.jpg" alt="Chocolate Recipes at 101 Cookbooks" data-pin-media="http://www.101cookbooks.com/mt-static/images/food/indian-spiced-guacamole.jpg" data-pin-description="Appetizer Recipes - Looking for great appetizer recipes? These are the best appetizer recipes from the award-winning 101 Cookbooks recipe journal. - from 101Cookbooks.com" data-pin-url="http://www.101cookbooks.com/archives/../appetizers/"></div> 


<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/this-is-how-you-step-up-your-guacamole-game-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/indian-spiced-guacamole.jpg" class="mainimg" alt="This is How You Step up Your Guacamole Game" title="This is How You Step up Your Guacamole Game" data-pin-url="http://www.101cookbooks.com/archives/this-is-how-you-step-up-your-guacamole-game-recipe.html" data-pin-description="This is How You Step up Your Guacamole Game - Imagine guacamole topped with fragrant, Indian-spiced onions and garlic, green chiles, and mustard seeds. It&#39;s great with chips, toasted naan, or toasted pita. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/this-is-how-you-step-up-your-guacamole-game-recipe.html">This is How You Step up Your Guacamole Game</a></h2><div class="entrydivider"></div></div>

<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/green-chile-whipped-goat-cheese-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/whipped-green-chile-goat-cheese.jpg" class="mainimg" alt="Green Chile Whipped Goat Cheese" title="Green Chile Whipped Goat Cheese" data-pin-url="http://www.101cookbooks.com/archives/green-chile-whipped-goat-cheese-recipe.html" data-pin-description="Green Chile Whipped Goat Cheese - This is the recipe I was working on when Sarah Lonsdale came to visit for an interview for Remodelista. Whipped goat cheese is simply combined with flecks of sautéed garlic and green chile resulting in an easy spread that pairs perfectly with a long list of things. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/green-chile-whipped-goat-cheese-recipe.html">Green Chile Whipped Goat Cheese</a></h2><div class="entrydivider"></div></div>

<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/seed-pata-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/seed_pate_recipe.jpg" class="mainimg" alt="Seed Pâté" title="Seed Pâté" data-pin-url="http://www.101cookbooks.com/archives/seed-pata-recipe.html" data-pin-description="Seed Pâté - Make this seed pâté when you want something in your refrigerator that can easily assimilate into just about any snack or meal. The base is made of seeds that have been soaked for a stretch and then blended into a creamy, full-bodied puree. In this instance I&#39;ve worked in fresh herbs and garlic, but it&#39;s not hard to imagine many different ways to approach the base. Finish seed pate with a bit of miso - for flavor, seasoning, and easy nutritional boost. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/seed-pata-recipe.html">Seed Pâté</a></h2><div class="entrydivider"></div></div>

<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/red-lentil-dumplings-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/red_lentil_dumplings.jpg" class="mainimg" alt="Red Lentil Dumplings" title="Red Lentil Dumplings" data-pin-url="http://www.101cookbooks.com/archives/red-lentil-dumplings-recipe.html" data-pin-description="Red Lentil Dumplings -  I have one more day in Hong Kong before flying back to San Francisco, and I thought I&#39;d jot off a quick post for those of you who like to see what I pack to eat when I travel. Red lentil dumplings flared out with roasted cherry tomatoes, and a simple smoked paprika and garlic oil. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/red-lentil-dumplings-recipe.html">Red Lentil Dumplings</a></h2><div class="entrydivider"></div></div>

<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/turmeric-cashews-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/turmeric-cashews.jpg" class="mainimg" alt="Turmeric Cashews" title="Turmeric Cashews" data-pin-url="http://www.101cookbooks.com/archives/turmeric-cashews-recipe.html" data-pin-description="Turmeric Cashews - Turmeric Cashews tossed with cayenne, nori, and sesame. Inspired by The Good Gut written by Stanford researchers Justin and Erica Sonnenburg. Keep your microbiota happy. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/turmeric-cashews-recipe.html">Turmeric Cashews</a></h2><div class="entrydivider"></div></div>


 

<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/superfood-tapenade-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/superfood-tapenade.jpg" class="mainimg" alt="Superfood Tapenade" title="Superfood Tapenade" data-pin-url="http://www.101cookbooks.com/archives/superfood-tapenade-recipe.html" data-pin-description="Superfood Tapenade - A superfood tapenade of sorts - green olives, walnuts, and good olive oil boosted with anise, and a bit of the wheatgrass powder I sometimes add to drinks or dressings - crumbled nori would be a great substitute. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/superfood-tapenade-recipe.html">Superfood Tapenade</a></h2><div class="entrydivider"></div></div>

<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/beet-caviar-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/beet_caviar_recipe.jpg" class="mainimg" alt="Beet Caviar" title="Beet Caviar" data-pin-url="http://www.101cookbooks.com/archives/beet-caviar-recipe.html" data-pin-description="Beet Caviar - Inspired by a loaf of 100% einkorn bread passed to me by a friend (and the cookbook by Silvena Rowe I had in my bag at the time) - a beet caviar. Perfect for slathering - sweet earthiness of roasted beets accented with toasted walnuts, chives, dates, and a swirl of creme fraiche. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/beet-caviar-recipe.html">Beet Caviar</a></h2><div class="entrydivider"></div></div>

<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/deviled-eggs-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/deviled_eggs_recipe.jpg" class="mainimg" alt="Deviled Eggs" title="Deviled Eggs" data-pin-url="http://www.101cookbooks.com/archives/deviled-eggs-recipe.html" data-pin-description="Deviled Eggs - A beautiful and delicious deviled egg recipe made with an herb-flecked filling and topped with toasted almonds. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/deviled-eggs-recipe.html">Deviled Eggs</a></h2><div class="entrydivider"></div></div>

<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/guacamole-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/guacamole_recipe.jpg" class="mainimg" alt="Guacamole" title="Guacamole" data-pin-url="http://www.101cookbooks.com/archives/guacamole-recipe.html" data-pin-description="Guacamole - To make great guacamole you have to go off-recipe. It&#39;s all about the in-between steps, decisions, and knowing when avocados are at their best. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/guacamole-recipe.html">Guacamole</a></h2><div class="entrydivider"></div></div>

<div class="post"><div class="mainimagewide"><a href="http://www.101cookbooks.com/archives/silverdollar-socca-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/silverdollar_socca_recipe.jpg" class="mainimg" alt="Silverdollar Socca" title="Silverdollar Socca" data-pin-url="http://www.101cookbooks.com/archives/silverdollar-socca-recipe.html" data-pin-description="Silverdollar Socca - Tiny socca-inspired pancakes made with buttermilk or keffir -  golden, tender, and cooked in cold-pressed spicy mustard oil. Flat out delicious. - from 101Cookbooks.com"></a></div>
<h2><a href="http://www.101cookbooks.com/archives/silverdollar-socca-recipe.html">Silverdollar Socca</a></h2><div class="entrydivider"></div></div>










  <nav class="text-center">
  <ul class="pagination"><li class="active"><a href="http://www.101cookbooks.com/appetizers/#">1 <span class="sr-only">(current)</span></a></li> <li><a href="http://www.101cookbooks.com/appetizers/page/2">2</a></li> <li><a href="http://www.101cookbooks.com/appetizers/page/3">3</a></li> <li><a href="http://www.101cookbooks.com/appetizers/page/4">4</a></li> <li><a href="http://www.101cookbooks.com/appetizers/page/5">5</a></li> <li> <a href="http://www.101cookbooks.com/appetizers/page/2" aria-label="Next" rel="next"> <span aria-hidden="true">»</span></a> </li> </ul>
     <div class="tagresults">Appetizer Recipes  | <a href="http://www.101cookbooks.com/archives.html">Full Recipe Index</a></div>
</nav>

<div class="aboutheidi row hidden-lg">		 
<div class="col-xs-6 col-sm-4">	 <a href="https://www.snapchat.com/add/heidijswanson"><img src="<?php echo get_template_directory_uri() ?>/assets/snapchat-heidi-swanson.gif" alt="Heidi Swanson" border="0" nopin="nopin" class="img-responsive" style="margin-left:7px;max-width: 130px;"></a><div class="socialicons"><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;twitter&#39;]);" href="https://twitter.com/#!/101Cookbooks"><img src="<?php echo get_template_directory_uri() ?>/assets/twitter.png" alt="connect on twitter" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;facebook&#39;]);" href="http://www.facebook.com/101cookbooks"><img src="<?php echo get_template_directory_uri() ?>/assets/facebook.png" alt="connect on facebook" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;instragram&#39;]);" href="http://instagram.com/heidijswanson/"><img src="<?php echo get_template_directory_uri() ?>/assets/instagram.png" alt="connect on instagram" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;pinterest&#39;]);" href="http://pinterest.com/heidiswanson/"><img src="<?php echo get_template_directory_uri() ?>/assets/pinterest.png" alt="connect on pinterest" width="20" height="20"></a><!-- = end socialicons = --></div></div><div class="col-xs-6 col-sm-8">Welcome to my site about cooking with whole, natural foods! I share recipes of mine and from favorite cookbooks, inspired life, <a href="http://www.101cookbooks.com/travel/">travels</a>, and <a href="http://www.101cookbooks.com/favorites_lists/">favorite things</a>. Please visit my <a href="http://www.quitokeeto.com/" onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;quitokeeto&#39;]);">culinary boutique</a>. &nbsp;<br><em> -<a href="http://www.101cookbooks.com/about/">Heidi Swanson</a></em>
</div></div> <br class="clear">	


<div id="nevermissarecipe" class="row"> 
	<div class="col-md-12"><div id="sign-up" class="col-sm-9 col-xs-12"> <div class="row">
<form action="http://101cookbooks.us3.list-manage1.com/subscribe/post?u=7ce250831f3a7c06b82a6c24c&amp;id=8e979ad22f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" role="form" novalidate="">
	     <div class="col-xs-7 pull-left col-xs-offset-1"><input type="email" value="" name="EMAIL" class="email form-control" id="mailinglistinput" placeholder="Your email here" required=""><div style="position: absolute; left: -5000px;"><input type="text" name="b_7ce250831f3a7c06b82a6c24c_8e979ad22f" value=""></div><!-- = end row = --></div>
          <div class="col-xs-3 pull-left"><input type="submit" value="Join" name="subscribe" id="mc-embedded-subscribe" alt="subscribe to our list" class="joinbutton" onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;centercol&#39;,&#39;mailinglist&#39;]);"></div></form>
          <div class="updates text-center col-xs-11">For new recipes &amp; inspirations</div>
          </div><!-- = end signup = --></div>    
	      <div id="connectlinks" class="col-sm-3 pull-right hidden-xs hidden-sm hidden-md"><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;centercol&#39;,&#39;twitter&#39;]);" href="https://twitter.com/#!/101Cookbooks"><img src="<?php echo get_template_directory_uri() ?>/assets/twitter.png" alt="connect on twitter" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;centercol&#39;,&#39;facebook&#39;]);" href="http://www.facebook.com/101cookbooks"><img src="<?php echo get_template_directory_uri() ?>/assets/facebook.png" alt="connect on facebook" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;centercol&#39;,&#39;instagram&#39;]);" href="http://instagram.com/heidijswanson/"><img src="<?php echo get_template_directory_uri() ?>/assets/instagram.png" alt="connect on instagram" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;centercol&#39;,&#39;pinterest&#39;]);" href="http://pinterest.com/heidiswanson/"><img src="<?php echo get_template_directory_uri() ?>/assets/pinterest.png" alt="connect on pinterest" width="20" height="20"></a></div>  
<!-- = end colmid12 = --></div>      
<br class="clear"><!-- = end nevermissarecipe = --></div>

<br class="clear"><div class="centercol720 hidden-xs hidden-sm hidden-md" style="visibility: visible; opacity: 1;"><script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/fpi.js"></script><iframe id="sovrn_ad_unit_299296_async" margin="0" padding="0" frameborder="0" width="728" height="90" scrolling="no" src="<?php echo get_template_directory_uri() ?>/assets/saved_resource(2).html" data-zid="299296" class="sovrn_ad_unit" style="margin: 0px; padding: 0px; border: 0px none; width: 728px; height: 90px; overflow: hidden;"></iframe></div>	
     <br class="clear">
			<div class="globaldivider"></div>
</div><!--/ maincontent -->
<!-- == begin ad column == -->		
<div id="adcolumn" class="col-lg-3 hidden-xs hidden-sm hidden-md" style="visibility: visible; opacity: 1;">
	 <div id="aboutheidi">		 
			 <img src="<?php echo get_template_directory_uri() ?>/assets/heidi_swanson_101_cookbooks.jpg" alt="Heidi Swanson" border="0" nopin="nopin"> <p>Welcome to my site about cooking with whole, natural foods. I highlight recipes from favorite cookbooks, as well as recipes I write myself - inspired by ingredients that intersect my life, <a href="http://www.101cookbooks.com/travel/">travels</a>, and <a href="http://www.101cookbooks.com/favorites_lists/">everyday interests</a>. Also, visit my <a href="http://www.quitokeeto.com/" onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;quitokeeto&#39;]);">culinary boutique</a>. &nbsp;<em> -<a href="http://www.101cookbooks.com/about/">Heidi Swanson</a></em> </p><h4><a href="http://pinterest.com/heidiswanson/" onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;pinterest&#39;]);">Follow me on Pinterest</a></h4>
			 <div class="socialicons"><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;twitter&#39;]);" href="https://twitter.com/#!/101Cookbooks"><img src="<?php echo get_template_directory_uri() ?>/assets/twitter.png" alt="connect on twitter" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;facebook&#39;]);" href="http://www.facebook.com/101cookbooks"><img src="<?php echo get_template_directory_uri() ?>/assets/facebook.png" alt="connect on facebook" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;instragram&#39;]);" href="http://instagram.com/heidijswanson/"><img src="<?php echo get_template_directory_uri() ?>/assets/instagram.png" alt="connect on instagram" width="20" height="20"></a><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;pinterest&#39;]);" href="http://pinterest.com/heidiswanson/"><img src="<?php echo get_template_directory_uri() ?>/assets/pinterest.png" alt="connect on pinterest" width="20" height="20"></a>
			 
			 <br>	<a href="https://www.snapchat.com/add/heidijswanson"><img src="<?php echo get_template_directory_uri() ?>/assets/snapchat-heidi-swanson.gif" border="0" alt="snapchat" width="120" nopin="nopin" class="snapchat"></a>
			 
			 
			 <!-- = end socialicons = --></div></div>	

<div class="globaldivider"></div>
<div id="adcollist"> 
<h4>Get Recipes &amp; Inspiration</h4>
     <div id="sign-up" class="col-sm-12"> 
	       <form action="http://101cookbooks.us3.list-manage1.com/subscribe/post?u=7ce250831f3a7c06b82a6c24c&amp;id=8e979ad22f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" role="form" novalidate="">
          <div class="col-sm-9 pull-left"><input type="email" value="" name="EMAIL" class="email form-control" id="mailinglistinput" placeholder="Your email here" required=""><div style="position: absolute; left: -5000px;"><input type="text" name="b_7ce250831f3a7c06b82a6c24c_8e979ad22f" value=""></div></div>
          <div class="col-sm-3 pull-left"><input type="submit" value="Join" name="subscribe" id="mc-embedded-subscribe" alt="subscribe to our list" class="joinbutton" onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;101social&#39;,&#39;adcol&#39;,&#39;mailinglist&#39;]);"></div>
          </form></div><!-- = end adcollist = --></div>
	<div class="globaldivider"></div>
		<h4>My Books</h4>

<a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;nf&#39;,&#39;amazon&#39;]);" href="http://www.amazon.com/Near-Far-Recipes-Inspired-Travel/dp/1607745496/heidiswanson-20"><img src="<?php echo get_template_directory_uri() ?>/assets/near-and-far-cover.jpg" alt="Near and Far by Heidi Swanson" border="0" data-pin-description="Near &amp; Far: Recipes Inspired by Home and Travel by Heidi Swanson - September 2015" data-pin-media="http://101cookbooks.com/mt-static/images/2015/near-and-far-cover-lg.jpg" class="nearfarcover"></a>
	<div id="mybooks">
<h3 class="nearfar"><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;nf&#39;,&#39;amazon&#39;]);" href="http://www.amazon.com/Near-Far-Recipes-Inspired-Travel/dp/1607745496/heidiswanson-20">Near &amp; Far</a></h3>
			<h3 class="nearfar2"><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;nf&#39;,&#39;amazon&#39;]);" href="http://www.amazon.com/Near-Far-Recipes-Inspired-Travel/dp/1607745496/heidiswanson-20">Recipes Inspired by Home and Travel</a></h3>
		<ul class="bookavailable nearfar">
		<li><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;nf&#39;,&#39;amazon&#39;]);" href="http://www.amazon.com/Near-Far-Recipes-Inspired-Travel/dp/1607745496/heidiswanson-20">Amazon</a> |</li>
		<li><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;nf&#39;,&#39;barnes&#39;]);" href="http://www.barnesandnoble.com/w/near-far-heidi-swanson/1121494955?ean=9781607749899">Barnes &amp; Noble (signed)</a> |</li>
		<li><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;nf&#39;,&#39;powells&#39;]);" href="https://www.powells.com/biblio/62-9781607745495-0">Powell's</a></li></ul>
		<div class="globaldivider"></div>
	<div class="SNED col-sm-12"><h3>Super Natural Every Day</h3><ul class="bookavailable">
		<li><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;sned&#39;,&#39;amazon&#39;]);" href="http://www.amazon.com/exec/obidos/ASIN/1580082777/heidiswanson-20">Amazon</a> |</li>
		<li><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;sned&#39;,&#39;barnes&#39;]);" href="http://search.barnesandnoble.com/books/e/9781580082778/">Barnes &amp; Noble</a> |</li>
		<li><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;sned&#39;,&#39;ibooks&#39;]);" href="http://itunes.apple.com/us/book/super-natural-every-day/id447341603?mt=11">iBooks</a></li>
		</ul>
	</div><div class="SNC col-sm-12">
	<h3>Super Natural Cooking</h3><ul class="bookavailable"> 
			<li><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;sned&#39;,&#39;amazon&#39;]);" href="http://www.amazon.com/exec/obidos/ASIN/1587612755/heidiswanson-20">Amazon</a> |</li>
			<li><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;sned&#39;,&#39;barnes&#39;]);" href="http://search.barnesandnoble.com/Super-Natural-Cooking/Heidi-Swanson/e/9781587612756">Barnes &amp; Noble</a> |</li>
			<li><a onclick="_gaq.push([&#39;_trackEvent&#39;,&#39;books&#39;,&#39;sned&#39;,&#39;powells&#39;]);" href="http://www.powells.com/biblio/1-9781587612756-0">Powell's</a></li>
		</ul></div>
	<div class="clear"></div></div><div class="globaldivider"></div>



	<h4 id="recently">Recently on 101 Cookbooks</h4>
<div id="recentlythumbs">
	<div class="recentlyrow row">
	<div class="thumbholder col-sm-5 pull-left"><div style="display: none;" class="thumb"><a href="http://www.101cookbooks.com/archives/travel-tucson-gem-show-recipe.html">Travel: Tucson Gem Show</a></div><a href="http://www.101cookbooks.com/archives/travel-tucson-gem-show-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/tucson-gem-show.jpg" alt="Travel: Tucson Gem Show" width="120" height="80" border="0" nopin="nopin"></a></div>
	<div class="thumbholder col-sm-5 pull-left"><div style="display: none;" class="thumb"><a href="http://www.101cookbooks.com/archives/healthful-double-chocolate-cookies-recipe.html">Healthful Double Chocolate Cookies</a></div><a href="http://www.101cookbooks.com/archives/healthful-double-chocolate-cookies-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/Healthful-Double-Chocolate-Cookies.jpg" alt="Healthful Double Chocolate Cookies" width="120" height="80" border="0" nopin="nopin"></a></div>
	<!-- = end row1 = --></div>
	<div class="recentlyrow row">	
		<div class="thumbholder col-sm-5 pull-left"><div style="display: none;" class="thumb"><a href="http://www.101cookbooks.com/archives/an-excellent-onepan-proteinpacked-power-pasta-recipe.html">An Excellent, One-pan, Protein-packed Power Pasta</a></div><a href="http://www.101cookbooks.com/archives/an-excellent-onepan-proteinpacked-power-pasta-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/one-pan-pasta.jpg" alt="An Excellent, One-pan, Protein-packed Power Pasta" width="120" height="80" border="0" nopin="nopin"></a></div>
	<div class="thumbholder col-sm-5 pull-left"><div style="display: none;" class="thumb"><a href="http://www.101cookbooks.com/archives/this-is-how-you-step-up-your-guacamole-game-recipe.html">This is How You Step up Your Guacamole Game</a></div><a href="http://www.101cookbooks.com/archives/this-is-how-you-step-up-your-guacamole-game-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/indian-spiced-guacamole(1).jpg" alt="This is How You Step up Your Guacamole Game" width="120" height="80" border="0" nopin="nopin"></a></div>		
		<!-- = end row2 = --></div>
	<div class="recentlyrow row">
<div class="thumbholder col-sm-5 pull-left"><div style="display: none;" class="thumb"><a href="http://www.101cookbooks.com/archives/how-to-make-the-creamy-toasted-coconut-milk-of-your-dreams-recipe.html">How To Make the Creamy, Toasted Coconut Milk of Your Dreams</a></div><a href="http://www.101cookbooks.com/archives/how-to-make-the-creamy-toasted-coconut-milk-of-your-dreams-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/toasted-coconut-milk.jpg" alt="How To Make the Creamy, Toasted Coconut Milk of Your Dreams" width="120" height="80" border="0" nopin="nopin"></a></div>
<div class="thumbholder col-sm-5 pull-left"><div style="display: none;" class="thumb"><a href="http://www.101cookbooks.com/archives/a-glowpromoting-luminizing-breakfast-beauty-bowl-recipe.html">A Glow-promoting, Luminizing Breakfast Beauty Bowl</a></div><a href="http://www.101cookbooks.com/archives/a-glowpromoting-luminizing-breakfast-beauty-bowl-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/breakfast-beauty-bowl.jpg" alt="A Glow-promoting, Luminizing Breakfast Beauty Bowl" width="120" height="80" border="0" nopin="nopin"></a></div>
	<!-- = end row3 = --></div>
	<div class="recentlyrow row">
		<div class="thumbholder col-sm-5 pull-left"><div style="display: none;" class="thumb"><a href="http://www.101cookbooks.com/archives/vibrant-vegan-double-broccoli-buddha-bowl-recipe.html">Vibrant Vegan Double Broccoli Buddha Bowl</a></div><a href="http://www.101cookbooks.com/archives/vibrant-vegan-double-broccoli-buddha-bowl-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/Broccoli-Buddha-Bowl.jpg" alt="Vibrant Vegan Double Broccoli Buddha Bowl" width="120" height="80" border="0" nopin="nopin"></a></div>
	<div class="thumbholder col-sm-5 pull-left"><div style="display: none;" class="thumb"><a href="http://www.101cookbooks.com/archives/pickled-turmeric-eggs-recipe.html">Pickled Turmeric Eggs</a></div><a href="http://www.101cookbooks.com/archives/pickled-turmeric-eggs-recipe.html"><img src="<?php echo get_template_directory_uri() ?>/assets/pickled-turmeric-eggs-h.jpg" alt="Pickled Turmeric Eggs" width="120" height="80" border="0" nopin="nopin"></a></div>
		<!-- = end row4 = --></div><br class="clear">
<!-- = end recentlythumbs = --></div>
<br class="clear">




	
<div class="globaldivider"></div>

</div><!-- = end housead div = -->




	
<br class="clear"></div><!--/adcol --><br class="clear"><footer><div class="legalfooter">Use of this site constitutes acceptance of its <a href="http://www.101cookbooks.com/useragreement.html">User Agreement</a> and <a href="http://www.101cookbooks.com/privacy.html">Privacy Policy</a>.
<br class="clear"></div>

</footer><!--/ footer -->


</div><!--/ container-fluid -->

</body></html>